# My Portfolio

## COLSON Dylan

I'm 24 years old and I started my journey in development in 2020, teaching myself HTML and CSS. In 2021, I enrolled in a web and mobile development training program. As part of the program, I completed a two-month internship at a company, and they offered me a work-study opportunity to earn the title of application developer designer, which I successfully obtained in November 2022.

# About

The portfolio project brings together several small applications like a todo list or a calculator. 
For the project, I am using React.js for the frontend development and Tailwind CSS for the styling.
The goal of the project is to gather all the challenges one could encounter in the world of front-end development and address them through various concrete projects. By tackling different problematics, such as creating a responsive layout, implementing complex UI components, or handling data fetching from APIs, this portfolio aims to showcase my skills and experience in front-end development using React.js and Tailwind CSS.

# Installation

To run this project locally, follow these steps:

- Clone the repository.
- Run npm install to install the dependencies.
- Execute npm start to start the application.
